package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
		if(!file.exists())
			return null;
		try{
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() == 0 || machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
			file.delete();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}/*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() == 0 || machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getOrigHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getOrigHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			accout.setName(comp.getString("username"));
			
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() == 0 || machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				
			}
			accout.setMachineAuth(new SteamMachineAuth(comp.hasKey("machineName") && comp.getString("machineName").equalsIgnoreCase("steamMachineAuth"), comp.getString("machineName"), comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.get);
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			accout.setName(comp.getString("username"));
			
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() == 0 || machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.get);
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getClient()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getOrigHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getOrigHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			accout.setOrigHttpConfig(httpConfig);
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getOrigHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getOrigHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getClient()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			accout.setOrigHttpConfig(httpConfig);
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getOrigHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getOrigHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			accout.setName(comp.getString("username"));
			
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() == 0 || machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() == 0 || machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	public static boolean SAVE_PASSWORD = true;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			if(SAVE_PASSWORD && comp.hasKey("password"))
				accout.setPassword(comp.getString("password"));
			String steamId = comp.hasKey("steamId") ? comp.getString("steamId") : "";
			System.out.println("SteamID: "+steamId);
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			if(machineAuth.length() != 0 && machineAuth.startsWith("steamMachineAuth") && machineAuth.replaceFirst("steamMachineAuth", "").length() != 0){
				steamId = machineAuth.substring("steamMachineAuth".length());
			}
			System.out.println("SteamID: "+steamId);
			accout.setSteamId(steamId);
			accout.setMachineAuth(new SteamMachineAuth(false, steamId, comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		System.out.println("Saving account "+account.getUsername().toLowerCase()+" - "+getIp(account.getOrigHttpConfig()));
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getOrigHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.getSteamId());
			if(SAVE_PASSWORD)
				comp.setString("password", account.getPassword());
				
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			accout.setName(comp.getString("username"));
			accout.setMachineAuth(new SteamMachineAuth(comp.getString("machineName"), comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				if(!file.getParentFile().mkdirs() || !file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                                                                                                                                                                                                                                                                 
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	public static boolean IS_CACHE_DISABLED = false;
	
	public boolean isAccountCached(String username,RequestConfig httpConfig){
		if(IS_CACHE_DISABLED)
			return false;
		if(username == null){
			System.err.println("Invalid username");
			return false;
		}
		if(httpConfig == null){
			System.err.println("Invalid config");
			return false;
		}
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(httpConfig).getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			accout.setName(comp.getString("username"));
			
			String machineAuth = comp.hasKey("machineName") ? comp.getString("machineName") : "";
			accout.setMachineAuth(new SteamMachineAuth(comp.hasKey("machineName") && comp.getString("machineName").equalsIgnoreCase("steamMachineAuth"), comp.getString("machineName"), comp.getString("machineValue")));
			accout.setHttpConfig(httpConfig);
			
			return accout;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(getIp(account.getHttpConfig()).getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				file.getParentFile().mkdirs();
				if(!file.createNewFile()){
					System.out.println("Cant create temp data file!");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			comp.setString("machineName", account.getMachineAuth().getName());
			comp.setString("machineValue", account.getMachineAuth().getValue());
			comp.setString("steamId", account.get);
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getIp(RequestConfig config){
		String ip = config.getProxy() == null || config.getProxy().getHostName() == null ? "0.0.0.0" : config.getProxy().getHostName().toString();
		return ip.toLowerCase();
	}
}
                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	
	public boolean isAccountCached(String username,InetAddress ip){
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(ip == null ? "0.0.0.0".getBytes() : ip.getHostAddress().getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			String sip = httpConfig.getProxy() == null ? "0.0.0.0" : httpConfig.getProxy().getAddress().getHostAddress().toString();
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(sip.getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			accout.setNickname(comp.getString("username"));
			accout.setHttpConfig(httpConfig);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		String ip = account.getHttpConfig().getProxy() == null ? "0.0.0.0" : account.getHttpConfig().getProxy().getAddress().getHostAddress().toString();
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(ip.getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				if(!file.getParentFile().mkdirs() || !file.createNewFile())
					return;
			} catch (IOException e) {
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		System.out.println(".");
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
*//*
-----------------------------------------------
package dev.wolveringer.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.client.config.RequestConfig;

import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.steam.SteamAccount;

public class SteamCache {
	private static SteamCache cache = new SteamCache();
	
	public static SteamCache getCache() {
		return cache;
	}
	
	public static final File BASE_CACHE_DIR = new File(".steamcache/");
	
	public boolean isAccountCached(String username,InetAddress ip){
		return new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(ip == null ? "0.0.0.0".getBytes() : ip.getHostAddress().getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache").exists();
	}
	
	public SteamAccount getCachedAccountInformation(String username,RequestConfig httpConfig){
		try{
			String sip = httpConfig.getProxy() == null ? "0.0.0.0" : httpConfig.getProxy().getAddress().getHostAddress().toString();
			File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(sip.getBytes())+"/"+DatatypeConverter.printBase64Binary(username.toLowerCase().getBytes())+".cache");
			if(!file.exists())
				return null;
			
			InputStream is;
			NBTTagCompound comp = NBTCompressedStreamTools.read(is = new FileInputStream(file));
			is.close();
			
			SteamAccount accout = SteamAccount.createAccount(null, null, null);
			accout.setBrowserId(comp.getString("browserId"));
			accout.setSessionId(comp.getString("sessionId"));
			accout.setLoginSecure(comp.getString("loginSecure"));
			accout.setRemomberToken(comp.getString("loginRememberToken"));
			accout.setUsername(comp.getString("username"));
			accout.setName(comp.getString("username"));
			accout.setHttpConfig(httpConfig);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveAccount(SteamAccount account){
		String ip = account.getHttpConfig().getProxy() == null ? "0.0.0.0" : account.getHttpConfig().getProxy().getAddress().getHostAddress().toString();
		File file = new File(BASE_CACHE_DIR,"users/"+DatatypeConverter.printBase64Binary(ip.getBytes())+"/"+DatatypeConverter.printBase64Binary(account.getUsername().toLowerCase().getBytes())+".cache");
		if(!file.exists())
			try {
				if(!file.getParentFile().mkdirs() || !file.createNewFile())
					return;
			} catch (IOException e) {
				return;
			}
		try {
			OutputStream os = new FileOutputStream(file);
			NBTTagCompound comp = new NBTTagCompound();
			comp.setString("username", account.getUsername());
			comp.setString("browserId", account.getBrowserId());
			comp.setString("sessionId", account.getSessionId());
			comp.setString("loginSecure", account.getLoginSecure());
			comp.setString("loginRememberToken", account.getRemomberToken());
			NBTCompressedStreamTools.write(comp, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		System.out.println(".");
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*/