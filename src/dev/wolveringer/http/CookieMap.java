package dev.wolveringer.http;

import java.util.HashMap;
import java.util.Map.Entry;

public class CookieMap {
	private HashMap<String, String> cookies = new HashMap<>();
	
	public HashMap<String, String> getCookies() {
		return cookies;
	}
	
	public String toHTMLHeader(){
		return toHTMLHeader(true);
	}
	public String toHTMLHeader(boolean addNull){
		StringBuilder out = new StringBuilder();
		for(Entry<String, String> e : cookies.entrySet())
			if(e.getValue() != null || addNull)
			out.append(e.getKey()+"="+e.getValue()+";");
		return out.toString();
	}
}/*
-----------------------------------------------
package dev.wolveringer.http;

import java.util.HashMap;
import java.util.Map.Entry;

public class CookieMap {
	private HashMap<String, String> cookies = new HashMap<>();
	
	public HashMap<String, String> getCookies() {
		return cookies;
	}
	
	public String toHTMLHeader(){
		StringBuilder out = new StringBuilder();
		for(Entry<String, String> e : cookies.entrySet())
			out.append(e.getKey()+"="+e.getValue()+";");
		return out.toString();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
*/