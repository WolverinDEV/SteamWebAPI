package dev.wolveringer.http;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.http.entity.mime.content.FileBody;

public class ImageBody extends FileBody {
	byte[] data;
	String name;

	public ImageBody(byte[] data, String name) {
		super(new File("./"));
		this.data = data;
		this.name = name;
	}

	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(data);
	}

	public void writeTo(OutputStream out) throws IOException {
		if (out == null) {
			throw new IllegalArgumentException("Output stream may not be null");
		}
		InputStream is = getInputStream();
		try {
			IOUtils.copy(is, out);
		} finally {
			is.close();
		}
	}

	public String getTransferEncoding() {
		return "binary";
	}

	@Override
	public String getCharset() {
		return Charset.defaultCharset().name();
	}

	public String getMimeType() {
		return "application/octet-stream";
	}

	public long getContentLength() {
		return data.length;
	}

	public String getFilename() {
		return name;
	}

	public File getFile() {
		return null;
	}

}/*
-----------------------------------------------
package dev.wolveringer.http;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.entity.mime.content.FileBody;

public class ImageBody extends FileBody {
	public static File TEMP_FILE = FileUtils.getTempDirectory();
	byte[] data;
	String name;

	public ImageBody(byte[] data, String name) {
		super(TEMP_FILE);
		this.data = data;
		this.name = name;
	}

	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(data);
	}

	public void writeTo(OutputStream out) throws IOException {
		if (out == null) {
			throw new IllegalArgumentException("Output stream may not be null");
		}
		InputStream is = getInputStream();
		try {
			IOUtils.copy(is, out);
		} finally {
			is.close();
		}
	}

	public String getTransferEncoding() {
		return "binary";
	}

	public Charset getCharset() {
		return null;
	}

	public String getMimeType() {
		return "application/octet-stream";
	}

	public long getContentLength() {
		return data.length;
	}

	public String getFilename() {
		return name;
	}

	public File getFile() {
		return null;
	}

}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.http;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.entity.mime.content.FileBody;

public class ImageBody extends FileBody {
	public static File TEMP_FILE = FileUtils.getTempDirectory();
	byte[] data;
	String name;

	public ImageBody(byte[] data, String name) {
		super(TEMP_FILE);
		this.data = data;
		this.name = name;
	}

	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(data);
	}

	public void writeTo(OutputStream out) throws IOException {
		if (out == null) {
			throw new IllegalArgumentException("Output stream may not be null");
		}
		InputStream is = getInputStream();
		try {
			IOUtils.copy(is, out);
		} finally {
			is.close();
		}
	}

	public String getTransferEncoding() {
		return "binary";
	}

	public Charset getCharset() {
		return null;
	}

	public String getMimeType() {
		return "application/octet-stream";
	}

	public long getContentLength() {
		return data.length;
	}

	public String getFilename() {
		return name;
	}

	public File getFile() {
		return null;
	}

}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*/