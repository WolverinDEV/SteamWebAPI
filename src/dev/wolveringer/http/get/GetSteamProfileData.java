package dev.wolveringer.http.get;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;

import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GetSteamProfileData {
	private final String profileId;
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String loginToken;
	private final String loginSecure;
	private final SteamMachineAuth auth;
	private final String steamRememberLogin;
	private final HttpClient client;
	private final RequestConfig config;
	private String response;
	public void get() throws Exception{
		System.out.println(profileId+":"+auth);
		HttpGet request = new HttpGet("https://steamcommunity.com/profiles/"+profileId+"/edit"); //http://steamcommunity.com/profiles/76561198312723341/edit
		request.addHeader("Cookie", "sessionid="+sessionId+"; steamLogin="+loginToken+"; steamLoginSecure="+loginSecure+"; "+auth.getName()+"="+auth.getValue()+"; steamRememberLogin="+steamRememberLogin+";");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		System.out.println(response);
		this.response = IOUtils.toString(stream);
		request.abort();
	}
	public String getResponse() {
		return response;
	}
}

/*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import dev.wolveringer.steam.AccountData;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GetSteamProfileData {
	private final String profileId;
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String loginToken;
	private final String loginSecure;
	private final SteamMachineAuth auth;
	private final String steamRememberLogin;
	private final HttpClient client;
	private final RequestConfig config;
	private String response;
	public void get() throws Exception{
		System.out.println(profileId);
		HttpGet request = new HttpGet("https://steamcommunity.com/profiles/"+profileId+"/edit"); //http://steamcommunity.com/profiles/76561198312723341/edit
		request.addHeader("Cookie", "sessionid="+sessionId+"; steamLogin="+loginToken+"; steamLoginSecure="+loginSecure+"; "+auth.getName()+"="+auth.getValue()+"; steamRememberLogin="+steamRememberLogin+";");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		System.out.println(response);
		this.response = IOUtils.toString(stream);
		request.abort();
	}
	public String getResponse() {
		return response;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                           
*//*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import dev.wolveringer.steam.AccountProfile;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GetSteamProfileData {
	private final String profileId;
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String loginToken;
	private final String loginSecure;
	private final SteamMachineAuth auth;
	private final String steamRememberLogin;
	private final HttpClient client;
	private final RequestConfig config;
	private String response;
	public void get() throws Exception{
		System.out.println(profileId);
		HttpGet request = new HttpGet("https://steamcommunity.com/profiles/"+profileId+"/edit");
		request.addHeader("Cookie", "sessionid="+sessionId+"; steamLogin="+loginToken+"; steamLoginSecure="+loginSecure+"; "+auth.getName()+"="+auth.getValue()+"; steamRememberLogin="+steamRememberLogin+";");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		System.out.println(response);
		this.response = IOUtils.toString(stream);
		request.abort();
	}
	public String getResponse() {
		return response;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import dev.wolveringer.steam.AccountData;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GetSteamProfileData {
	private final String profileId;
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String loginToken;
	private final String loginSecure;
	private final SteamMachineAuth auth;
	private final String steamRememberLogin;
	private final HttpClient client;
	private final RequestConfig config;
	private String response;
	public void get() throws Exception{
		System.out.println(profileId);
		HttpGet request = new HttpGet("https://steamcommunity.com/id/"+profileId+"/edit");
		request.addHeader("Cookie", "sessionid="+sessionId+"; steamLogin="+loginToken+"; steamLoginSecure="+loginSecure+"; "+auth.getName()+"="+auth.getValue()+"; steamRememberLogin="+steamRememberLogin+";");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		System.out.println(response);
		this.response = IOUtils.toString(stream);
		request.abort();
	}
	public String getResponse() {
		return response;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
*/