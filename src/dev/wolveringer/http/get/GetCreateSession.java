package dev.wolveringer.http.get;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetCreateSession {
	private final HttpClient client;
	private final RequestConfig config;
	private String sessionId;
	private String browserId;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("http://store.steampowered.com");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		for(Header h : response.getHeaders("Set-Cookie")){
			if(h.getValue().startsWith("sessionid="))
				sessionId = h.getValue().replaceFirst("sessionid=", "").split(";")[0];
			if(h.getValue().startsWith("browserid="))
				browserId = h.getValue().replaceFirst("browserid=", "").split(";")[0];
		}
		request.abort();
	}
	public String getSessionId() {
		return sessionId;
	}
	public String getBrowserId() {
		return browserId;
	}
}/*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetCreateSession {
	private final HttpClient client;
	private final RequestConfig config;
	private String sessionId;
	private String browserId;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("http://store.steampowered.com");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		System.out.println(response);
		for(Header h : response.getHeaders("Set-Cookie")){
			if(h.getValue().startsWith("sessionid="))
				sessionId = h.getValue().replaceFirst("sessionid=", "").split(";")[0];
			if(h.getValue().startsWith("browserid="))
				browserId = h.getValue().replaceFirst("browserid=", "").split(";")[0];
		}
		request.abort();
	}
	public String getSessionId() {
		return sessionId;
	}
	public String getBrowserId() {
		return browserId;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
*//*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetCreateSession {
	private final HttpClient client;
	private final RequestConfig config;
	private String sessionId;
	private String browserId;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("http://store.steampowered.com");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		for(Header h : response.getHeaders("Set-Cookie")){
			if(h.getValue().startsWith("sessionid="))
				sessionId = h.getValue().replaceFirst("sessionid=", "").split(";")[0];
			if(h.getValue().startsWith("browserid="))
				browserId = h.getValue().replaceFirst("browserid=", "").split(";")[0];
		}
		request.abort();
	}
	public String getSessionId() {
		return sessionId;
	}
	public String getBrowserId() {
		return browserId;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
*//*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetCreateSession {
	private final HttpClient client;
	private final RequestConfig config;
	private String sessionId;
	private String browserId;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("http://store.steampowered.com");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		for(Header h : response.getHeaders("Set-Cookie")){
			if(h.getValue().startsWith("sessionid="))
				sessionId = h.getValue().replaceFirst("sessionid=", "").split(";")[0];
			if(h.getValue().startsWith("browserid="))
				browserId = h.getValue().replaceFirst("browserid=", "").split(";")[0];
		}
		request.abort();
	}
	public String getSessionId() {
		return sessionId;
	}
	public String getBrowserId() {
		return browserId;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetCreateSession {
	private final HttpClient client;
	private final RequestConfig config;
	private String sessionId;
	private String browserId;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("http://store.steampowered.com");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		for(Header h : response.getHeaders("Set-Cookie")){
			if(h.getValue().startsWith("sessionid="))
				sessionId = h.getValue().replaceFirst("sessionid=", "").split(";")[0];
			if(h.getValue().startsWith("browserid="))
				browserId = h.getValue().replaceFirst("browserid=", "").split(";")[0];
		}
		request.abort();
	}
	public String getSessionId() {
		return sessionId;
	}
	public String getBrowserId() {
		return browserId;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
*/