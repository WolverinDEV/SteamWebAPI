package dev.wolveringer.http.get;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamCaptcha {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String id;
	private final HttpClient client;
	private final RequestConfig config;
	private BufferedImage response;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("https://steamcommunity.com/login/rendercaptcha/?gid="+id);
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		this.response = ImageIO.read(stream);
		request.abort();
	}
	public BufferedImage getResponse() {
		return response;
	}

}
class MyComponent extends JComponent {
	ImageIcon icon;

	public MyComponent(ImageIcon icon) {
		super();
		this.icon = icon;
	}

	public void draw() {
		JFrame frame = new JFrame();

		frame.add(this);
		frame.setSize(icon.getIconWidth()+100, icon.getIconHeight()+100);
		frame.setVisible(true);
		setLocation(50, 50);
		setSize(icon.getIconWidth(),icon.getIconHeight());
		setVisible(true);
	}

	public void paint(Graphics g) {
		int x = 0;
		int y = 0;
		icon.paintIcon(this, g, x, y);
	}
}/*
-----------------------------------------------
package dev.wolveringer.http.get;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamCaptcha {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String id;
	private final HttpClient client;
	private final RequestConfig config;
	private BufferedImage response;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("https://steamcommunity.com/login/rendercaptcha/?gid="+id);
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		this.response = ImageIO.read(stream);
		request.abort();
	}
	public BufferedImage getResponse() {
		return response;
	}

}
class MyComponent extends JComponent {
	ImageIcon icon;

	public MyComponent(ImageIcon icon) {
		super();
		this.icon = icon;
	}

	public void draw() {
		JFrame frame = new JFrame();

		frame.add(this);
		frame.setSize(icon.getIconWidth()+100, icon.getIconHeight()+100);
		frame.setVisible(true);
		setLocation(50, 50);
		setSize(icon.getIconWidth(),icon.getIconHeight());
		setVisible(true);
	}

	public void paint(Graphics g) {
		int x = 0;
		int y = 0;
		icon.paintIcon(this, g, x, y);
	}
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
*/