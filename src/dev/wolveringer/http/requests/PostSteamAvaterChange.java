package dev.wolveringer.http.requests;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.ImageScaler;
import dev.wolveringer.http.ImageBody;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostSteamAvaterChange {
	private final String sessionId;
	private final String steamLogin;
	private final String steamLoginRemeber;
	private final String username;
	private final String steamId;
	private final File newImage;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;

	@SuppressWarnings("deprecation")
	public void post() throws Exception {
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("avatar", new ImageBody(resizeImage(newImage), newImage.getName()));
		entity.addPart("type", new StringBody("player_avatar_image"));
		entity.addPart("sId", new StringBody(steamId));
		entity.addPart("sessionid", new StringBody(sessionId));
		entity.addPart("steamRememberLogin", new StringBody(steamLoginRemeber == null ? "" : steamLoginRemeber));
		entity.addPart("doSub", new StringBody("1"));
		entity.addPart("json", new StringBody("1"));

		HttpPost request = new HttpPost("http://steamcommunity.com/actions/FileUploader");

		request.setEntity(entity);
		request.addHeader("Cookie", "sessionid=" + sessionId + "; steamLogin=" + steamLogin + ";");
		request.addHeader("Referer", "Http://steamcommunity.com/id/" + username + "/edit");
		request.setConfig(config);

		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");

		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	private byte[] resizeImage(File image) throws IOException {
		byte[] data = IOUtils.toByteArray(new FileInputStream(image));
		ImageScaler scaled = new ImageScaler(ImageIO.read(new ByteArrayInputStream(data)));
		scaled.createScaledImage(184, ImageScaler.ScalingDirection.VERTICAL);
		ImageIcon scaledImage = scaled.getScaledImage();
		BufferedImage buImg = new BufferedImage(scaledImage.getIconWidth(), scaledImage.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		buImg.getGraphics().drawImage(scaledImage.getImage(), 0, 0, scaledImage.getImageObserver());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(buImg, "jpg", baos);
		byte[] imageInByte = baos.toByteArray();
		System.out.println("Old image size: " + (data.length / 1024) + "kb new size: " + (imageInByte.length / 1024) + "kb");
		return imageInByte;
	}

	private InputStream decompress(InputStream is) throws IOException {
		return new GZIPInputStream(is);
	}

	public JSONObject getResponse() {
		return response;
	}
}/*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.ImageScaler;
import dev.wolveringer.ImageScaler.ScalingDirection;
import dev.wolveringer.http.ImageBody;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostSteamAvaterChange {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String steamLogin; //76561198229845108%7C%7C7DADB1921AA41FE007434620EFC22D4F6AD0EA62
	private final String steamLoginRemeber;
	private final String username;
	private final String steamId;
	private final File newImage;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void post() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("avatar", new ImageBody(resizeImage(newImage), newImage.getName()));
		entity.addPart("type", new StringBody("player_avatar_image"));
		entity.addPart("sId", new StringBody(steamId));
		entity.addPart("sessionid", new StringBody(sessionId));
		entity.addPart("steamRememberLogin", new StringBody(steamLoginRemeber == null ? "": steamLoginRemeber));//steamRememberLogin=76561198229845108%7C%7C4dbc10a374bbd9e2036ff71813950dd9
		entity.addPart("doSub", new StringBody("1"));
		entity.addPart("json", new StringBody("1"));

		HttpPost request = new HttpPost("http://steamcommunity.com/actions/FileUploader");

		request.setEntity(entity);
		request.addHeader("Cookie", "sessionid="+sessionId+"; steamLogin="+steamLogin+";");
		request.addHeader("Referer", "Http://steamcommunity.com/id/"+username+"/edit");
		request.setConfig(config);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			stream = decompress(stream);
		String out = IOUtils.toString(stream, "UTF-8");
		
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	private byte[] resizeImage(File image) throws IOException {
		byte[] data = IOUtils.toByteArray(new FileInputStream(image));
		ImageScaler scaled = new ImageScaler(ImageIO.read(new ByteArrayInputStream(data)));
		scaled.createScaledImage(184, ImageScaler.ScalingDirection.VERTICAL);
		ImageIcon scaledImage = scaled.getScaledImage();
		//new MyComponent(scaledImage).draw();
		BufferedImage buImg = new BufferedImage(scaledImage.getIconWidth(), scaledImage.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		buImg.getGraphics().drawImage(scaledImage.getImage(), 0, 0, scaledImage.getImageObserver());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(buImg, "jpg", baos);
		byte[] imageInByte = baos.toByteArray();
		System.out.println("Old image size: " + (data.length / 1024) + "kb new size: " + (imageInByte.length / 1024) + "kb");
		//new MyComponent(new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageInByte)))).draw();
		return imageInByte;
	}

	private InputStream decompress(InputStream is) throws IOException {
		return new GZIPInputStream(is);
	}
	
	public JSONObject getResponse() {
		return response;
	}
}
                                              
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.ImageScaler;
import dev.wolveringer.ImageScaler.ScalingDirection;
import dev.wolveringer.http.ImageBody;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostSteamAvaterChange {
	private final String sessionId;
	private final String steamLogin;
	private final String steamLoginRemeber;
	private final String username;
	private final String steamId;
	private final File newImage;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void post() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("avatar", new ImageBody(resizeImage(newImage), newImage.getName()));
		entity.addPart("type", new StringBody("player_avatar_image"));
		entity.addPart("sId", new StringBody(steamId));
		entity.addPart("sessionid", new StringBody(sessionId));
		entity.addPart("steamRememberLogin", new StringBody(steamLoginRemeber == null ? "": steamLoginRemeber));
		entity.addPart("doSub", new StringBody("1"));
		entity.addPart("json", new StringBody("1"));

		HttpPost request = new HttpPost("http://steamcommunity.com/actions/FileUploader");

		request.setEntity(entity);
		request.addHeader("Cookie", "sessionid="+sessionId+"; steamLogin="+steamLogin+";");
		request.addHeader("Referer", "Http://steamcommunity.com/id/"+username+"/edit");
		request.setConfig(config);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			stream = decompress(stream);
		String out = IOUtils.toString(stream, "UTF-8");
		
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	private byte[] resizeImage(File image) throws IOException {
		byte[] data = IOUtils.toByteArray(new FileInputStream(image));
		ImageScaler scaled = new ImageScaler(ImageIO.read(new ByteArrayInputStream(data)));
		scaled.createScaledImage(184, ImageScaler.ScalingDirection.VERTICAL);
		ImageIcon scaledImage = scaled.getScaledImage();
		//new MyComponent(scaledImage).draw();
		BufferedImage buImg = new BufferedImage(scaledImage.getIconWidth(), scaledImage.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		buImg.getGraphics().drawImage(scaledImage.getImage(), 0, 0, scaledImage.getImageObserver());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(buImg, "jpg", baos);
		byte[] imageInByte = baos.toByteArray();
		System.out.println("Old image size: " + (data.length / 1024) + "kb new size: " + (imageInByte.length / 1024) + "kb");
		//new MyComponent(new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageInByte)))).draw();
		return imageInByte;
	}

	private InputStream decompress(InputStream is) throws IOException {
		return new GZIPInputStream(is);
	}
	
	public JSONObject getResponse() {
		return response;
	}
}
                                                                                                                                                                                                                       
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.ImageBody;
import dev.wolveringer.ImageScaler;
import dev.wolveringer.ImageScaler.ScalingDirection;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostSteamAvaterChange {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String steamLogin; //76561198229845108%7C%7C7DADB1921AA41FE007434620EFC22D4F6AD0EA62
	private final String steamLoginRemeber;
	private final String username;
	private final String steamId;
	private final File newImage;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void post() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("avatar", new ImageBody(resizeImage(newImage), newImage.getName()));
		entity.addPart("type", new StringBody("player_avatar_image"));
		entity.addPart("sId", new StringBody(steamId));
		entity.addPart("sessionid", new StringBody(sessionId));
		entity.addPart("steamRememberLogin", new StringBody(steamLoginRemeber == null ? "": steamLoginRemeber));//steamRememberLogin=76561198229845108%7C%7C4dbc10a374bbd9e2036ff71813950dd9
		entity.addPart("doSub", new StringBody("1"));
		entity.addPart("json", new StringBody("1"));

		HttpPost request = new HttpPost("http://steamcommunity.com/actions/FileUploader");

		request.setEntity(entity);
		request.addHeader("Cookie", "sessionid="+sessionId+"; steamLogin="+steamLogin+";");
		request.addHeader("Referer", "Http://steamcommunity.com/id/"+username+"/edit");
		request.setConfig(config);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			stream = decompress(stream);
		String out = IOUtils.toString(stream, "UTF-8");
		
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	private byte[] resizeImage(File image) throws IOException {
		byte[] data = IOUtils.toByteArray(new FileInputStream(image));
		ImageScaler scaled = new ImageScaler(ImageIO.read(new ByteArrayInputStream(data)));
		scaled.createScaledImage(184, ImageScaler.ScalingDirection.VERTICAL);
		ImageIcon scaledImage = scaled.getScaledImage();
		//new MyComponent(scaledImage).draw();
		BufferedImage buImg = new BufferedImage(scaledImage.getIconWidth(), scaledImage.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		buImg.getGraphics().drawImage(scaledImage.getImage(), 0, 0, scaledImage.getImageObserver());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(buImg, "jpg", baos);
		byte[] imageInByte = baos.toByteArray();
		System.out.println("Old image size: " + (data.length / 1024) + "kb new size: " + (imageInByte.length / 1024) + "kb");
		//new MyComponent(new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageInByte)))).draw();
		return imageInByte;
	}

	private InputStream decompress(InputStream is) throws IOException {
		return new GZIPInputStream(is);
	}
	
	public JSONObject getResponse() {
		return response;
	}
}
                                                   
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.ImageScaler;
import dev.wolveringer.ImageScaler.ScalingDirection;
import dev.wolveringer.http.ImageBody;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostSteamAvaterChange {
	private final String sessionId;
	private final String steamLogin;
	private final String steamLoginRemeber;
	private final String username;
	private final String steamId;
	private final File newImage;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;

	public void post() throws Exception {
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("avatar", new ImageBody(resizeImage(newImage), newImage.getName()));
		entity.addPart("type", new StringBody("player_avatar_image"));
		entity.addPart("sId", new StringBody(steamId));
		entity.addPart("sessionid", new StringBody(sessionId));
		entity.addPart("steamRememberLogin", new StringBody(steamLoginRemeber == null ? "" : steamLoginRemeber));
		entity.addPart("doSub", new StringBody("1"));
		entity.addPart("json", new StringBody("1"));

		HttpPost request = new HttpPost("http://steamcommunity.com/actions/FileUploader");

		request.setEntity(entity);
		request.addHeader("Cookie", "sessionid=" + sessionId + "; steamLogin=" + steamLogin + ";");
		request.addHeader("Referer", "Http://steamcommunity.com/id/" + username + "/edit");
		request.setConfig(config);

		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			stream = decompress(stream);
		String out = IOUtils.toString(stream, "UTF-8");

		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	private byte[] resizeImage(File image) throws IOException {
		byte[] data = IOUtils.toByteArray(new FileInputStream(image));
		ImageScaler scaled = new ImageScaler(ImageIO.read(new ByteArrayInputStream(data)));
		scaled.createScaledImage(184, ImageScaler.ScalingDirection.VERTICAL);
		ImageIcon scaledImage = scaled.getScaledImage();
		//new MyComponent(scaledImage).draw();
		BufferedImage buImg = new BufferedImage(scaledImage.getIconWidth(), scaledImage.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		buImg.getGraphics().drawImage(scaledImage.getImage(), 0, 0, scaledImage.getImageObserver());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(buImg, "jpg", baos);
		byte[] imageInByte = baos.toByteArray();
		System.out.println("Old image size: " + (data.length / 1024) + "kb new size: " + (imageInByte.length / 1024) + "kb");
		//new MyComponent(new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageInByte)))).draw();
		return imageInByte;
	}

	private InputStream decompress(InputStream is) throws IOException {
		return new GZIPInputStream(is);
	}

	public JSONObject getResponse() {
		return response;
	}
}
                                                                                                                                                                                                             
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.ImageScaler;
import dev.wolveringer.ImageScaler.ScalingDirection;
import dev.wolveringer.http.ImageBody;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostSteamAvaterChange {
	private final String sessionId;
	private final String steamLogin;
	private final String steamLoginRemeber;
	private final String username;
	private final String steamId;
	private final File newImage;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;

	public void post() throws Exception {
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("avatar", new ImageBody(resizeImage(newImage), newImage.getName()));
		entity.addPart("type", new StringBody("player_avatar_image"));
		entity.addPart("sId", new StringBody(steamId));
		entity.addPart("sessionid", new StringBody(sessionId));
		entity.addPart("steamRememberLogin", new StringBody(steamLoginRemeber == null ? "" : steamLoginRemeber));
		entity.addPart("doSub", new StringBody("1"));
		entity.addPart("json", new StringBody("1"));

		HttpPost request = new HttpPost("http://steamcommunity.com/actions/FileUploader");

		request.setEntity(entity);
		request.addHeader("Cookie", "sessionid=" + sessionId + "; steamLogin=" + steamLogin + ";");
		request.addHeader("Referer", "Http://steamcommunity.com/id/" + username + "/edit");
		request.setConfig(config);

		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			stream = decompress(stream);
		String out = IOUtils.toString(stream, "UTF-8");

		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	private byte[] resizeImage(File image) throws IOException {
		byte[] data = IOUtils.toByteArray(new FileInputStream(image));
		ImageScaler scaled = new ImageScaler(ImageIO.read(new ByteArrayInputStream(data)));
		scaled.createScaledImage(184, ImageScaler.ScalingDirection.VERTICAL);
		ImageIcon scaledImage = scaled.getScaledImage();
		//new MyComponent(scaledImage).draw();
		BufferedImage buImg = new BufferedImage(scaledImage.getIconWidth(), scaledImage.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		buImg.getGraphics().drawImage(scaledImage.getImage(), 0, 0, scaledImage.getImageObserver());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(buImg, "jpg", baos);
		byte[] imageInByte = baos.toByteArray();
		System.out.println("Old image size: " + (data.length / 1024) + "kb new size: " + (imageInByte.length / 1024) + "kb");
		//new MyComponent(new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageInByte)))).draw();
		return imageInByte;
	}

	private InputStream decompress(InputStream is) throws IOException {
		return new GZIPInputStream(is);
	}

	public JSONObject getResponse() {
		return response;
	}
}
                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.ImageScaler;
import dev.wolveringer.http.ImageBody;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostSteamAvaterChange {
	private final String sessionId;
	private final String steamLogin;
	private final String steamLoginRemeber;
	private final String username;
	private final String steamId;
	private final File newImage;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;

	public void post() throws Exception {
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("avatar", new ImageBody(resizeImage(newImage), newImage.getName()));
		entity.addPart("type", new StringBody("player_avatar_image"));
		entity.addPart("sId", new StringBody(steamId));
		entity.addPart("sessionid", new StringBody(sessionId));
		entity.addPart("steamRememberLogin", new StringBody(steamLoginRemeber == null ? "" : steamLoginRemeber));
		entity.addPart("doSub", new StringBody("1"));
		entity.addPart("json", new StringBody("1"));

		HttpPost request = new HttpPost("http://steamcommunity.com/actions/FileUploader");

		request.setEntity(entity);
		request.addHeader("Cookie", "sessionid=" + sessionId + "; steamLogin=" + steamLogin + ";");
		request.addHeader("Referer", "Http://steamcommunity.com/id/" + username + "/edit");
		request.setConfig(config);

		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			stream = decompress(stream);
		String out = IOUtils.toString(stream, "UTF-8");

		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	private byte[] resizeImage(File image) throws IOException {
		byte[] data = IOUtils.toByteArray(new FileInputStream(image));
		ImageScaler scaled = new ImageScaler(ImageIO.read(new ByteArrayInputStream(data)));
		scaled.createScaledImage(184, ImageScaler.ScalingDirection.VERTICAL);
		ImageIcon scaledImage = scaled.getScaledImage();
		//new MyComponent(scaledImage).draw();
		BufferedImage buImg = new BufferedImage(scaledImage.getIconWidth(), scaledImage.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		buImg.getGraphics().drawImage(scaledImage.getImage(), 0, 0, scaledImage.getImageObserver());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(buImg, "jpg", baos);
		byte[] imageInByte = baos.toByteArray();
		System.out.println("Old image size: " + (data.length / 1024) + "kb new size: " + (imageInByte.length / 1024) + "kb");
		//new MyComponent(new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageInByte)))).draw();
		return imageInByte;
	}

	private InputStream decompress(InputStream is) throws IOException {
		return new GZIPInputStream(is);
	}

	public JSONObject getResponse() {
		return response;
	}
}
                                                                                                                                                                                                                                                                                                   
*/