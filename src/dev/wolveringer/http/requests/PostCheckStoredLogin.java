package dev.wolveringer.http.requests;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class PostCheckStoredLogin {
	private final String loginSecure;
	private final String rememberToken;
	private final String sessionId;
	private final String browserId;
	private final SteamMachineAuth auth;
	private final HttpClient client;
	private final RequestConfig config;
	private Header[] headers;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("https://store.steampowered.com/login/checkstoredlogin/?redirectURL=0");
		request.addHeader("Cookie", (sessionId == null ? "" : "sessionid="+sessionId+"; ")+"browserid="+browserId+"; "+(loginSecure == null ? "" : "steamLoginSecure="+loginSecure+";")+"steamRememberLogin="+rememberToken+";"+(auth == null ? "" : auth.toString()));
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		//InputStream stream = response.getEntity().getContent();
		headers = response.getAllHeaders();
		request.abort();
	}
}/*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class PostCheckStoredLogin {
	private final String loginSecure;
	private final String rememberToken;
	private final String sessionId;
	private final String browserId;
	private final SteamMachineAuth auth;
	private final HttpClient client;
	private final RequestConfig config;
	private Header[] headers;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("https://store.steampowered.com/login/checkstoredlogin/?redirectURL=0");
		request.addHeader("Cookie", (sessionId == null ? "" : "sessionid="+sessionId+"; ")+"browserid="+browserId+"; "+(loginSecure == null ? "" : "steamLoginSecure="+loginSecure+";")+"steamRememberLogin="+rememberToken+";"+auth.toString());
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		headers = response.getAllHeaders();
		request.abort();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;

import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class PostCheckStoredLogin {
	private final String loginSecure;
	private final String rememberToken;
	private final String sessionId;
	private final String browserId;
	private final SteamMachineAuth auth;
	private final HttpClient client;
	private final RequestConfig config;
	private Header[] headers;
	
	public void get() throws Exception{
		HttpPost request = new HttpPost("https://store.steampowered.com/login/checkstoredlogin/?redirectURL=0");
		request.addHeader("Cookie", (sessionId == null ? "" : "sessionid="+sessionId+"; ")+"browserid="+browserId+"; "+(loginSecure == null ? "" : "steamLoginSecure="+loginSecure+";")+"steamRememberLogin="+rememberToken+";"+auth.getName()+"="+auth.getValue());
		request.setConfig(config);
		HttpResponse response = client.execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		headers = response.getAllHeaders();
		request.abort();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*/