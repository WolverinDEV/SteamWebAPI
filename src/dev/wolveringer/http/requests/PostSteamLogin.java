package dev.wolveringer.http.requests;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.naming.OperationNotSupportedException;
import javax.print.DocFlavor.STRING;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.SteamAccount.SteamMachineAuth;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}/*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                            
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                 
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private static final HttpHost URL = new HttpHost("https://steamcommunity.com/login/dologin/");
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(new HttpHost("https://steamcommunity.com/login/dologin/"), request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                       
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                     
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                        
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                                                                                                                                       
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		client.execute(new HttpHost("https://steamcommunity.com/login/dologin/", request);
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                             
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = client.execute(new HttpHost("https://steamcommunity.com/login/dologin/"), request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		client.execute(new HttpHost("https://steamcommunity.com/login/dologin/"), request);
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                            
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private static final HttpHost URL = new HttpHost("https://steamcommunity.com/login/dologin/");
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(URL, request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                             
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private static final HttpHost URL = new HttpHost("https://steamcommunity.com/login/dologin/");
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(new HttpHost("https://steamcommunity.com/login/dologin/"), request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                       
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                                                                                                                                       
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                     
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                        
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = client.execute(new HttpHost("https://steamcommunity.com/login/dologin/"), request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                 
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		client.execute(new HttpHost("https://steamcommunity.com/login/dologin/", request);
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                             
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private static final HttpHost URL = new HttpHost("https://steamcommunity.com/login/dologin/");
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost(URL);
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private static final HttpHost URL = new HttpHost("https://steamcommunity.com/login/dologin/");
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost();
		request.setEntity(entity);
		request.setConfig(config);
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		CloseableHttpResponse response = (CloseableHttpResponse) client.execute(URL, request);
		System.out.println("Done");
		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
		response.close();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                             
*//*
-----------------------------------------------
package dev.wolveringer.http.requests;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import dev.wolveringer.LoginState;
import dev.wolveringer.http.CookieMap;
import dev.wolveringer.steam.encription.SteamRSAKey;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor(access = AccessLevel.MODULE)
public class PostSteamLogin {
	private String sessionId = null;
	private String username = null;
	private String password = null;
	private String twofactorcode = "";
	private SteamRSAKey rsaKey = null;
	private String captchaText = "";
	private String captchaId = "";
	private String emailsteamid;
	private String emailauth = "";
	private boolean rememberLogin = true;

	private String steamLogin;
	private String steamLoginSecure;
	private String browserId;
	
	private HttpClient client;
	private RequestConfig config;

	private JSONObject response;
	private Header[] headers;

	public void post() throws Exception {
		if (sessionId == null || username == null || password == null || rsaKey == null)
			throw new NullPointerException("Missing informations ("+sessionId+" - "+username+" - "+password+" - "+rsaKey+")");
		if (client == null)
			client = HttpClientBuilder.create().build();
		if (config == null)
			config = RequestConfig.DEFAULT;
		String encodedpassword = rsaKey.transformPassword(password);
		System.out.println("Password: "+encodedpassword);
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringNullBody(String.valueOf(System.currentTimeMillis()+1000)));
		entity.addPart("username", new StringNullBody(username.toLowerCase()));
		entity.addPart("password", new StringNullBody(encodedpassword));
		entity.addPart("twofactorcode", new StringNullBody(twofactorcode));
		entity.addPart("emailauth", new StringNullBody(emailauth));
		entity.addPart("loginfriendlyname", new StringNullBody(""));
		entity.addPart("captchagid", new StringNullBody(captchaId));
		entity.addPart("captcha_text", new StringNullBody(captchaText));
		entity.addPart("emailsteamid", new StringNullBody(emailsteamid));
		entity.addPart("rsatimestamp", new StringNullBody(String.valueOf(rsaKey.getTimestamp())));
		entity.addPart("remember_login", new StringNullBody(String.valueOf(rememberLogin)));
		
		HttpPost request = new HttpPost("https://steamcommunity.com/login/dologin/");
		request.setEntity(entity);
		request.setConfig(config);
		
		CookieMap cookies = new CookieMap();
		cookies.getCookies().put("sessionid", sessionId);
		cookies.getCookies().put("browserid", browserId);
		cookies.getCookies().put("steamLogin", steamLogin);
		cookies.getCookies().put("steamLoginSecure",steamLoginSecure);
		request.addHeader("Cookie", cookies.toHTMLHeader(false));
		
		request.addHeader("Referer","https://steamcommunity.com/login/home/?goto=0");
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);

		headers = response.getAllHeaders();
		InputStream stream = response.getEntity().getContent();
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		} catch (JSONException e) {
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}

	public String getMessage() {
		return response.has("message") ? response.getString("message") : "undefined";
	}

	public boolean isLoginBlocked() {
		return getMessage().toLowerCase().contains("too many login failures");
	}

	public JSONObject getResponse() {
		return response;
	}

	public boolean isTwofactorIdentification() {
		return response.has("requires_twofactor") && response.getBoolean("requires_twofactor");
	}

	public boolean isIncorrectPassword() {
		return response.has("clear_password_field") && response.getBoolean("clear_password_field");
	}

	public boolean isSuccessful() {
		return response.getBoolean("success");
	}

	public boolean isLoggedin() {
		return response.has("login_complete") && response.getBoolean("login_complete");
	}

	public boolean isCatchaNeeded() {
		return response.has("captcha_needed") && response.getBoolean("captcha_needed");
	}

	public String getCaptchaId() {
		return response.getString("captcha_gid");
	}
	
	public boolean isEmailAuth(){
		return response.has("emailauth_needed") && response.getBoolean("emailauth_needed");
	}

	public Header[] getHeaders() {
		return headers;
	}

	public LoginState getState() {
		if (isSuccessful())
			return LoginState.SUCCESS;
		else if (isIncorrectPassword())
			return LoginState.WRONG_PASSWORD;
		else if (isTwofactorIdentification())
			return LoginState.TWO_FACTOR_AUTH;
		else if (isCatchaNeeded())
			return LoginState.WRONG_CAPTCHA;
		else if (isLoginBlocked())
			return LoginState.LOGIN_BLOCKED;
		else if(isEmailAuth())
			return LoginState.EMAIL_AUTH;
		return LoginState.UNKNOWN;
	}
	
}

class StringNullBody extends StringBody {
	private static final String NULL_STRING = "";

	public StringNullBody(String text, Charset charset) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text, charset);
	}

	public StringNullBody(String text) throws UnsupportedEncodingException {
		super(text == null ? NULL_STRING : text);
	}

}                                                                                                                                                                                                                                                                                                                                    
*/