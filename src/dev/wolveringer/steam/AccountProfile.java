package dev.wolveringer.steam;

import java.io.File;
import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import dev.wolveringer.http.get.GetSteamProfileData;
import dev.wolveringer.http.requests.PostSteamAvaterChange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountProfile {
	public static boolean AUTOLOAD = true;
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}
	
	public boolean setAvatar(File file) throws Exception {
		PostSteamAvaterChange change = new PostSteamAvaterChange(handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(),handle.getUsername() ,handle.getSteamId(), file, handle.getClient(), handle.getHttpConfig());
		change.post();
		if (change.getResponse().getBoolean("success"));
		else
			System.out.println("Cant change -> Exception: " + change.getResponse());
		return change.getResponse().getBoolean("success");
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}/*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.File;
import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import dev.wolveringer.http.get.GetSteamProfileData;
import dev.wolveringer.http.requests.PostSteamAvaterChange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountProfile {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	protected void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}
	
	public boolean setAvatar(File file) throws Exception {
		PostSteamAvaterChange change = new PostSteamAvaterChange(handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(),handle.getUsername() ,handle.getSteamId(), file, handle.getClient(), handle.getHttpConfig());
		change.post();
		if (change.getResponse().getBoolean("success"));
		else
			System.out.println("Cant change -> Exception: " + change.getResponse());
		return change.getResponse().getBoolean("success");
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.File;
import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import dev.wolveringer.http.get.GetSteamProfileData;
import dev.wolveringer.http.requests.PostSteamAvaterChange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountProfile {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	protected void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}
	
	public boolean setAvatar(File file) throws Exception {
		PostSteamAvaterChange change = new PostSteamAvaterChange(handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(),handle.getUsername() ,handle.getSteamId(), file, handle.getClient(), handle.getHttpConfig());
		change.post();
		if (change.getResponse().getBoolean("success"));
		else
			System.out.println("Cant change -> Exception: " + change.getResponse());
		return change.getResponse().getBoolean("success");
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.File;
import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import dev.wolveringer.http.get.GetSteamProfileData;
import dev.wolveringer.http.requests.PostSteamAvaterChange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountProfile {
	public static boolean AUTOLOAD = true;
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	protected void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}
	
	public boolean setAvatar(File file) throws Exception {
		PostSteamAvaterChange change = new PostSteamAvaterChange(handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(),handle.getUsername() ,handle.getSteamId(), file, handle.getClient(), handle.getHttpConfig());
		change.post();
		if (change.getResponse().getBoolean("success"));
		else
			System.out.println("Cant change -> Exception: " + change.getResponse());
		return change.getResponse().getBoolean("success");
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                          
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.File;
import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import dev.wolveringer.http.get.GetSteamProfileData;
import dev.wolveringer.http.requests.PostSteamAvaterChange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountProfile {
	public static boolean AUTOLOAD = true;
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	protected void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}
	
	public boolean setAvatar(File file) throws Exception {
		PostSteamAvaterChange change = new PostSteamAvaterChange(handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(),handle.getUsername() ,handle.getSteamId(), file, handle.getClient(), handle.getHttpConfig());
		change.post();
		if (change.getResponse().getBoolean("success"));
		else
			System.out.println("Cant change -> Exception: " + change.getResponse());
		return change.getResponse().getBoolean("success");
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                          
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountProfile {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.File;
import java.io.InputStream;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import dev.wolveringer.http.get.GetSteamProfileData;
import dev.wolveringer.http.requests.PostSteamAvaterChange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountProfile {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}
	
	public boolean setAvatar(File file) throws Exception {
		PostSteamAvaterChange change = new PostSteamAvaterChange(handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(),handle.getUsername() ,handle.getSteamId(), file, handle.getClient(), handle.getHttpConfig());
		change.post();
		if (change.getResponse().getBoolean("success"));
		else
			System.out.println("Cant change -> Exception: " + change.getResponse());
		return change.getResponse().getBoolean("success");
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
*/