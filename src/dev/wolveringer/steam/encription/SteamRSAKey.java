package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.util.Random;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if ((out.length() & 1) == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
}/*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if (out.length() % 1 == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if (out.length() % 1 == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			System.out.println((byte) (Math.floor(new Random().nextDouble() * 254) + 1));
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if (out.length() % 1 == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if ((out.length() & 1) == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.util.Random;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if ((out.length() & 1) == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.util.Random;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		System.out.println("Expon: "+encryptionExponent.toString(16));
		System.out.println("Modulus: "+modulus.toString(16));
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if ((out.length() & 1) == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.util.Random;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		System.out.println("Expon: "+encryptionExponent.toString(16));
		System.out.println("Modulus: "+modulus.toString(16));
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if ((out.length() & 1) == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = 0x00;//(byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.util.Random;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;

	public String transformPassword(String in) throws Exception {
		System.out.println("Expon: "+encryptionExponent.toString(16));
		System.out.println("Modulus: "+modulus.toString(16));
		BigInteger data = pkcs1pad2(in, (modulus.bitLength() + 7) >> 3);
		data = data.modPow(encryptionExponent, modulus);
		String out = data.toString(16);
		if ((out.length() & 1) == 1)
			out = "0" + out;
		return DatatypeConverter.printBase64Binary(Hex.decodeHex(out.toCharArray()));
	}

	private BigInteger pkcs1pad2(String data, int keysize) {
		if (keysize < data.length() + 11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length() - 1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble() * 254) + 1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}

}
/.*
var RSA = {

getPublicKey: function( $modulus_hex, $exponent_hex ) {
	return new RSAPublicKey( $modulus_hex, $exponent_hex );
},

encrypt: function($data, $pubkey) {
	if (!$pubkey) return false;
	$data = this.pkcs1pad2($data,($pubkey.modulus.bitLength()+7)>>3);
	if(!$data) return false;
	$data = $data.modPowInt($pubkey.encryptionExponent, $pubkey.modulus);
	if(!$data) return false;
	$data = $data.toString(16);
	if(($data.length & 1) == 1)
		$data = "0" + $data;
	return Base64.encode(Hex.decode($data));
},

pkcs1pad2: function($data, $keysize) {
	if($keysize < $data.length + 11)
		return null;
	var $buffer = [];
	var $i = $data.length - 1;
	while($i >= 0 && $keysize > 0)
		$buffer[--$keysize] = $data.charCodeAt($i--);
	$buffer[--$keysize] = 0;
	while($keysize > 2)
		$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
	$buffer[--$keysize] = 2;
	$buffer[--$keysize] = 0;
	return new BigInteger($buffer);
}
};

*./                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.PublicKey;
import java.util.Base64;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding","BC");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.PublicKey;
import java.util.Base64;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS12");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS12");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = data.charAt(i--);
		}
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS12");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS12");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS12");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		
		final Cipher cipher = Cipher.getInstance("RSA/None/PKCS12");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return buffer;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                                                                                                           
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private long timestamp;
	
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		System.exit(-1);
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		//byte[] data = pkcs1pad2(in, keysize)
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return buffer;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                                       
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private BigInteger mudulus;
	private long timestamp;
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		//byte[] data = pkcs1pad2(in, keysize)
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return buffer;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private BigInteger mudulus;
	private long timestamp;
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		//byte[] data = pkcs1pad2(in, keysize)
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return buffer;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                                  
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private BigInteger mudulus;
	private long timestamp;
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] data = pkcs1pad2(in, (mudulus.bitLength()+7)>>3);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return buffer;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                                                                                                
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] data = pkcs1pad2(in, (mudulus.bitLength()+7)>>3);
		
		mudulus.modPow(key.g, m)
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return buffer;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                                          
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] data = pkcs1pad2(in, (modulus.bitLength()+7)>>3);
		mudulus.modPow(encryptionExponent, modulus);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return buffer;
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] data = pkcs1pad2(in, (modulus.bitLength()+7)>>3);
		mudulus.modPow(encryptionExponent, modulus);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private byte[] pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.KeyStore.ProtectionParameter;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SteamRSAKey {
	private PublicKey key;
	private BigInteger encryptionExponent;
	private BigInteger modulus;
	private long timestamp;
	public String transformPassword(String in) throws Exception{
		///None/PKCS1Padding
		System.out.println(key.getFormat());
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] data = pkcs1pad2(in, (modulus.bitLength()+7)>>3);
		mudulus.modPow(encryptionExponent, modulus);
		String encodedpassword = Base64.getEncoder().encodeToString(cipher.doFinal(in.getBytes()));
		return encodedpassword;
	}

	private BigInteger pkcs1pad2(String data,int keysize){
		if(keysize < data.length()+11)
			return null;
		byte[] buffer = new byte[keysize];
		int i = data.length()-1;
		while (i >= 0 && keysize > 0) {
			buffer[--keysize] = (byte) data.charAt(i--);
		}
		buffer[--keysize] = 0;
		while (keysize > 2) {
			buffer[--keysize] = (byte) (Math.floor(new Random().nextDouble()*254)+1);
		}
		buffer[--keysize] = 2;
		buffer[--keysize] = 0;
		return new BigInteger(buffer);
	}
	
}
/.*
 * function($data, $keysize) {
			if($keysize < $data.length + 11)
				return null;
			var $buffer = [];
			var $i = $data.length - 1;
			while($i >= 0 && $keysize > 0)
				$buffer[--$keysize] = $data.charCodeAt($i--);
			$buffer[--$keysize] = 0;
			while($keysize > 2)
				$buffer[--$keysize] = Math.floor(Math.random()*254) + 1;
			$buffer[--$keysize] = 2;
			$buffer[--$keysize] = 0;
			return new BigInteger($buffer);
		}
		*./
                                                                                                                                                                                                     
*/