package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		System.out.println(response.getStatusLine());
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
}
/*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		System.out.println(response.getStatusLine());
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!response.has("publickey_mod")){
			System.out.println(response);
		}
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!response.has("publickey_mod")){
			System.out.println(response);
		}
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!response.has("publickey_mod")){
			System.out.println(response);
		}
		System.out.println("Rsa: "+response);
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!response.has("publickey_mod")){
			System.out.println(response);
		}
		System.out.println("Rsa: "+response);
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		System.out.println(username);
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!response.has("publickey_mod")){
			System.out.println(response);
		}
		System.out.println("Rsa: "+response);
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!response.has("publickey_mod")){
			System.out.println(response);
		}
		System.out.println("Rsa: "+response);
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public BigInteger getModulus(){
		return new BigInteger(response.getString("publickey_mod"),16);
	}
	public BigInteger getExponent(){
		return new BigInteger(response.getString("publickey_exp"),16);
	}
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam.encription;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetSteamRSAKey {
	private final String sessionId; //0a504fab96735eb845722b9c
	private final String username;
	private final HttpClient client;
	private final RequestConfig config;
	private JSONObject response;
	
	public void get() throws Exception{
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("donotcache", new StringBody(String.valueOf(System.currentTimeMillis())));
		entity.addPart("username", new StringBody(username));

		HttpPost request = new HttpPost("https://steamcommunity.com/login/getrsakey/");
		request.setEntity(entity);
		
		request.addHeader("Cookie", "sessionid="+sessionId+";");
		request.setConfig(config);
		//request.addHeader("Referer", referer);
		
		System.out.println("Exceute");
		HttpResponse response = client.execute(request);
		
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream, "UTF-8");
		try {
			this.response = new JSONObject(out);
		}catch(JSONException e){
			this.response = new JSONObject();
			this.response.put("success", "false");
			this.response.put("message", out);
		}
		request.abort();
	}
	
	public String getTimestamp(){
		return response.getString("timestamp");
	}
	
	public boolean successful(){
		return response.getBoolean("success");
	}
	
	public PublicKey getPublicKey() throws IOException, GeneralSecurityException {
		if(!response.has("publickey_mod")){
			System.out.println(response);
		}
		System.out.println("Rsa: "+response);
		if(!successful())
			return null;
		RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(response.getString("publickey_mod"),16), new BigInteger(response.getString("publickey_exp"),16));
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    return factory.generatePublic(spec);
    }
	
	public String getKey(){
		return response.getBoolean("success") ? response.getString("publickey_mod") : null;
	}
	
}
                                                                         
*/