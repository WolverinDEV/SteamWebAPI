package dev.wolveringer.steam;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.apache.http.Header;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;

import dev.wolveringer.LoginState;
import dev.wolveringer.cache.SteamCache;
import dev.wolveringer.http.get.GetCreateSession;
import dev.wolveringer.http.requests.PostCheckStoredLogin;
import dev.wolveringer.http.requests.PostSteamLogin;
import dev.wolveringer.steam.encription.GetSteamRSAKey;
import dev.wolveringer.steam.encription.SteamRSAKey;
import dev.wolveringer.steam.login.EmailAuthentifikation;
import dev.wolveringer.steam.login.LoginCaptcha;
import dev.wolveringer.steam.login.TwoFactorAuthentifikation;
import dev.wolveringer.steam.login.UserAuthentifikation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SteamAccount {
	public static SteamAccount createAccount(String username, String password, RequestConfig httpConfig) {
		if (username == null && password == null && httpConfig == null)
			return new SteamAccount(null, null, null);
		SteamAccount account = null;
		if (SteamCache.getCache().isAccountCached(username, httpConfig)) {
			account = SteamCache.getCache().getCachedAccountInformation(username, httpConfig);
			System.out.println("Use cached account information: " + account);
		}
		if (account == null)
			account = new SteamAccount(username, password, httpConfig);
		account.setPassword(password);
		return account;
	}

	@AllArgsConstructor
	@Getter
	public static class SteamMachineAuth {
		private boolean raw;
		private String name;
		private String value;

		@Override
		public String toString() {
			return (raw ? "" : "steamMachineAuth") + name + "=" + value;
		}
	}

	private String sessionId;
	private String browserId;

	private String steamId;

	private String loginToken;
	private String loginSecure;
	private String remomberToken;
	private SteamMachineAuth machineAuth;

	private AccountProfile profile = new AccountProfile(this);

	private String username;
	private String password;

	@Setter
	private HttpClient client;
	@Setter
	private RequestConfig httpConfig = RequestConfig.DEFAULT;

	private SteamAccount(String username, String password, RequestConfig httpConfig) {
		this.username = username;
		this.password = password;
		this.httpConfig = httpConfig;
		this.client = HttpClientBuilder.create().disableCookieManagement().setProxy(httpConfig == null ? RequestConfig.DEFAULT.getProxy() : httpConfig.getProxy()).build();
		this.httpConfig = RequestConfig.DEFAULT;
	}

	public void createSession() throws Exception {
		if (sessionId != null && browserId != null && machineAuth != null) {
			try {
				loginToken = null;
				reactivateLoginToken();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (loginToken != null)
				return;
		}
		GetCreateSession create = new GetCreateSession(client, httpConfig);
		create.get();
		sessionId = create.getSessionId();
		browserId = create.getBrowserId();
		if (sessionId == null || browserId == null)
			System.out.println(sessionId + " - " + browserId + " - ");
	}

	public PostSteamLogin login() throws Exception {
		return login(null, null);
	}

	public PostSteamLogin login(UserAuthentifikation auth, LoginCaptcha captcha) throws Exception {
		SteamRSAKey key = createPublicRSAKey();
		if (key == null)
			throw new RuntimeException("Cant create RSA key");
		PostSteamLogin.PostSteamLoginBuilder loginBuilder = PostSteamLogin.builder();
		/*
		 * if(loginSecure != null && loginToken != null){ //Email Auth
		 * loginBuilder.steamLogin(loginToken);
		 * loginBuilder.steamLoginSecure(loginSecure); }
		 */
		loginBuilder.config(httpConfig);
		loginBuilder.rsaKey(key);
		if (auth != null)
			auth.applay(loginBuilder);
		if (captcha != null) {
			loginBuilder.captchaText(captcha.getText());
			loginBuilder.captchaId(captcha.getId());
		} else
			loginBuilder.captchaId("-1");
		loginBuilder.sessionId(sessionId);
		loginBuilder.browserId(browserId);
		loginBuilder.username(username).password(password);
		loginBuilder.rememberLogin(true);
		PostSteamLogin login = loginBuilder.build();
		login.post();
		if (login.isSuccessful() || login.isEmailAuth()) {
			System.out.println("Paradise HTML Headers");
			for (Header h : login.getHeaders())
				if (h.getName().equalsIgnoreCase("Set-Cookie")) {
					String cookieName = h.getValue().split(";")[0].split("=")[0];
					String cookieValue = h.getValue().split(";")[0].split("=")[1];
					if (cookieName.equalsIgnoreCase("steamRememberLogin"))
						remomberToken = cookieValue;
					else if (cookieName.equalsIgnoreCase("steamLogin"))
						loginToken = cookieValue;
					else if (cookieName.equalsIgnoreCase("steamLoginSecure"))
						loginSecure = cookieValue;
				}
			if (login.isSuccessful()) {
				steamId = login.getResponse().getJSONObject("transfer_parameters").getString("steamid");
				machineAuth = new SteamMachineAuth(false, steamId, login.getResponse().getJSONObject("transfer_parameters").getString("webcookie"));
				/*
				 * loginSecure =
				 * login.getResponse().getJSONObject("transfer_parameters").
				 * getString("token_secure"); loginToken =
				 * login.getResponse().getJSONObject("transfer_parameters").
				 * getString("token");
				 */
			}
		}
		return login;
	}

	public void reactivateLoginToken() throws Exception {
		PostCheckStoredLogin check = new PostCheckStoredLogin(loginSecure, remomberToken, sessionId, browserId, machineAuth, client, httpConfig);
		check.get();
		System.out.println(Arrays.asList(check.getHeaders()));
		for (Header h : check.getHeaders())
			if (h.getName().equalsIgnoreCase("Set-Cookie")) {
				String cookieName = h.getValue().split(";")[0].split("=")[0];
				String cookieValue = h.getValue().split(";")[0].split("=")[1];
				if (cookieName.equalsIgnoreCase("steamLogin"))
					loginToken = cookieValue;
				else if (cookieName.equalsIgnoreCase("steamLoginSecure"))
					loginSecure = cookieValue;
				else if(cookieName.equalsIgnoreCase("steamRememberLogin") && cookieValue.equalsIgnoreCase("deleted")){
					remomberToken = null;
					loginSecure = null;
				}
			}
	}

	private SteamRSAKey createPublicRSAKey() throws Exception {
		GetSteamRSAKey request = new GetSteamRSAKey(sessionId, username, HttpClientBuilder.create().build(), httpConfig);
		request.get();
		if (!request.successful()) {
			System.out.println(request.getResponse());
			return null;
		}
		return new SteamRSAKey(request.getExponent(), request.getModulus(), Long.parseLong(request.getTimestamp()));
	}
	
	public boolean hasCachedLoginData(){
		return remomberToken != null;
	}

	public static String out = null;

	public static void main(String[] args) throws Exception {
		SteamAccount account = SteamAccount.createAccount("encoderexception", "Tz3z2106", RequestConfig.DEFAULT);
		account.createSession();
		System.out.println("SessionId -> " + account.sessionId);
		System.out.println("BrowserId -> " + account.browserId);

		LoginState state = account.loginToken == null ? LoginState.UNKNOWN : LoginState.SUCCESS;
		UserAuthentifikation auth = null;
		LoginCaptcha captcha = null;
		PostSteamLogin login = null;

		while (state != LoginState.SUCCESS) {
			switch (state) {
			case TWO_FACTOR_AUTH:
				System.out.println("Please enter twofactorauthcode:");
				auth = new TwoFactorAuthentifikation(readLine());
				break;
			case WRONG_CAPTCHA:
				/*
				 * System.out.println("CaptchaId: " + login.getCaptchaId());
				 * GetSteamCaptcha getCaptcha = new
				 * GetSteamCaptcha(account.getSessionId(), login.getCaptchaId(),
				 * account.getClient(), RequestConfig.DEFAULT); out = null;
				 * JFrame frame = new JFrame(); frame.setSize(500, 500);
				 * frame.setVisible(true); gui = new GuiCaptcha() {
				 * 
				 * @Override public void captchaEntered(String c) {
				 * System.out.println("Input -> " + c); gui.setVisible(false);
				 * frame.dispose(); out = c; } }; frame.add(gui);
				 * gui.setVisible(true); getCaptcha.get(); gui.setCaptcha(new
				 * ImageIcon(getCaptcha.getResponse())); while (out == null) {
				 * Thread.sleep(50); } captcha = new
				 * LoginCaptcha(login.getCaptchaId(), out); System.out.println(
				 * "X -> " + captcha.getText());
				 */
				System.out.println("Cant captcha");
				System.exit(-1);
				break;
			case WRONG_PASSWORD:
				System.out.println("Wrong password. (Hardcoded yet!)");
				System.out.println("Message: " + login.getResponse());
				System.exit(-1);
			case EMAIL_AUTH:
				System.out.println("Email code:");
				auth = new EmailAuthentifikation(login.getResponse().getString("emailsteamid"), readLine());
				break;
			case LOGIN_BLOCKED:
				System.out.println("To many logins!");
				System.exit(-1);
			}
			System.out.println("State: " + state);
			state = (login = account.login(auth, captcha)).getState();
		}

		System.out.println("State: " + state);
		if (login != null)
			System.out.println("Response: " + login.getResponse());
		System.out.println("Auth: " + account.machineAuth);
		System.out.println("LoginToke: " + account.loginToken);
		System.out.println("LoginSecure: " + account.loginSecure);
		System.out.println("RememberToken: " + account.remomberToken);
		/*
		 * account.loginToken = null; account.reactivateLoginToken();
		 * System.out.println("LoginToke: " + account.loginToken);
		 * System.out.println("LoginSecure: " + account.loginSecure);
		 * System.out.println("RememberToken: " + account.remomberToken);
		 * account.setAvatar(Main.getRandomFile()); System.out.println(
		 * "Avater set");
		 */
		account.saveAccount();

		account.getProfile().setNickname("11");
		account.getProfile().saveData();
		System.out.print(account.getProfile());
	}

	public AccountProfile getProfile() {
		if (profile.getNickname() == null && AccountProfile.AUTOLOAD) {
			try {
				profile.request();
			} catch (Exception e) {
				System.out.println("Cant request profile!");
				e.printStackTrace();
			}
		}
		return profile;
	}

	public void saveAccount() {
		SteamCache.getCache().saveAccount(this);
	}

	public String getNickname() {
		return "undefined";
	}

	private static String readLine() throws Exception {
		return new BufferedReader(new InputStreamReader(System.in)).readLine();
	}
}