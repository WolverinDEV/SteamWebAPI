package dev.wolveringer.steam;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;
	@Setter
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*/
	}
}/*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;
	@Setter
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").hasText());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;
	@Setter
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                          
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc);
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                              
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import dev.wolveringer.ImageBody;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	private String nickname;
	private String realname;
	private String urlName;
	private String country;
	private String summary;
	private String primaryGroupId;
	private String visiableBadgeId;
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public void saveData() {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/"+realname+"/edit");
		request.addHeader("Cookie", "sessionid="+handle.getSessionId()+"; steamLogin="+handle.getLoginToken()+"; steamRememberLogin="+handle.getRemomberToken()+";");
		request.setConfig(handle.getHttpConfig());
		
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));
		
		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));
		
		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));
		
		entity.addPart("customURL", new StringBody(urlName));
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		
		
		request.abort();
		
		
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;
	@Setter
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                           
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc.getElementById("personaName"));
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                     
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc);
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                   
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc.getAllElements());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                  
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		for(Element e : doc.getAllElements())
			System.out.println(e.id());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                       
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc);
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                              
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc.getElementById("personaName"));
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                     
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc);
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                   
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc);
		//nickname = doc.getElementById("personalName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		System.out.println(doc);
		nickname = doc.getElementById("personalName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		//privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                  
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import dev.wolveringer.ImageBody;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	private String nickname;
	private String realname;
	private String urlName;
	private String country;
	private String summary;
	private String primaryGroupId;
	private String visiableBadgeId;
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public void saveData() {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/"+realname+"/edit");
		request.addHeader("Cookie", "sessionid="+handle.getSessionId()+"; steamLogin="+handle.getLoginToken()+"; steamRememberLogin="+handle.getRemomberToken()+";");
		request.setConfig(handle.getHttpConfig());
		
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));
		
		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));
		
		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));
		
		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));
		
		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		
		
		request.abort();
		
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	private String nickname;
	private String realname;
	private String urlName;
	private String country;
	private String summary;
	private String primaryGroupId;
	private String visiableBadgeId;
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public void saveData() {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/"+realname+"/edit");
		request.addHeader("Cookie", "sessionid="+handle.getSessionId()+"; steamLogin="+handle.getLoginToken()+"; steamRememberLogin="+handle.getRemomberToken()+";");
		request.setConfig(handle.getHttpConfig());
		
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));
		
		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));
		
		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));
		
		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));
		
		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		request.abort();
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;
	@Setter
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public void saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));

		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		System.out.println(IOUtils.toString(stream));
		request.abort();
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                                                                                                                                                                                              
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.http.get.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getSteamId(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + handle.getSteamId() + "]";
	}

	public boolean saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/profiles/" + handle.getSteamId() + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		String out = IOUtils.toString(stream);
		Document cod = Jsoup.parse(out);
		request.abort();
		if(cod.getElementById("errorText") != null && cod.getElementById("errorText").hasText()) {
			System.out.println(" -> "+cod.getElementById("errorText").text());
			return false;
		}
		return true;
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	private String nickname;
	private String realname;
	private String urlName;
	private String country;
	private String summary;
	private String primaryGroupId;
	private String visiableBadgeId;
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public void saveData() {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/"+realname+"/edit");
		request.addHeader("Cookie", "sessionid="+handle.getSessionId()+"; steamLogin="+handle.getLoginToken()+"; steamRememberLogin="+handle.getRemomberToken()+";");
		request.setConfig(handle.getHttpConfig());
		
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));
		
		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));
		
		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));
		
		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));
		
		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		request.abort();
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	@Setter
	private String nickname;
	@Setter
	private String realname;
	@Setter
	private String urlName;
	@Setter
	private String country;
	@Setter
	private String summary;
	@Setter
	private String primaryGroupId;
	@Setter
	private String visiableBadgeId;
	@Setter
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	

		privateSteamId = getVariable(doc, "g_steamID");
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator();

		Pattern p = Pattern.compile("(?is)" + value + " = \"(.+?)\"");
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}

	public void saveData() throws Exception {
		HttpPost request = new HttpPost("http://steamcommunity.com/id/" + realname + "/edit");
		request.addHeader("Cookie", "sessionid=" + handle.getSessionId() + "; steamLogin=" + handle.getLoginToken() + "; steamRememberLogin=" + handle.getRemomberToken() + ";");
		request.setConfig(handle.getHttpConfig());

		MultipartEntity entity = new MultipartEntity();
		entity.addPart("sessionID", new StringBody(handle.getSessionId()));
		entity.addPart("type", new StringBody("profileSave"));

		entity.addPart("weblink_1_title", new StringBody(""));
		entity.addPart("weblink_1_url", new StringBody(""));
		entity.addPart("weblink_2_title", new StringBody(""));
		entity.addPart("weblink_2_url", new StringBody(""));
		entity.addPart("weblink_3_title", new StringBody(""));
		entity.addPart("weblink_3_url", new StringBody(""));
		entity.addPart("personaName", new StringBody(nickname));
		entity.addPart("real_name", new StringBody(realname));

		entity.addPart("country", new StringBody(""));
		entity.addPart("state", new StringBody(""));
		entity.addPart("city", new StringBody(""));

		entity.addPart("customURL", new StringBody(urlName));
		entity.addPart("summary", new StringBody(summary));

		entity.addPart("favorite_badge_badgeid", new StringBody(visiableBadgeId));
		entity.addPart("primary_group_steamid", new StringBody(primaryGroupId));
		request.setEntity(entity);
		
		HttpResponse response = handle.getClient().execute(request);
		InputStream stream = response.getEntity().getContent();
		if (response.getHeaders("Content-Encoding").length == 1 && response.getHeaders("Content-Encoding")[0].getValue().equalsIgnoreCase("gzip"))
			throw new OperationNotSupportedException("Cant decode response!");
		System.out.println(IOUtils.toString(stream));
		request.abort();
		/.*
		sessionID:fb6aad6a553212937a070620
		type:profileSave
		weblink_1_title:
		weblink_1_url:
		weblink_2_title:
		weblink_2_url:
		weblink_3_title:
		weblink_3_url:
		personaName:WolverinGER
		real_name:WolverinGER
		country:
		state:
		city:
		customURL:WolverinGER
		summary:Keine Informationen angegeben.
		favorite_badge_badgeid:
		favorite_badge_communityitemid:
		primary_group_steamid:0
		*./
	}
}
                                                                                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AccountData {
	private final SteamAccount handle;

	private String nickname;
	private String realname;
	private String urlName;
	private String country;
	private String summary;
	private String primaryGroupId;
	private String visiableBadgeId;
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		primaryGroupId = doc.getElementById("primary_group_steamid").val();

		privateSteamId = getVariable(doc, "g_steamID");
		//g_steamID = "76561198309269484";
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator(); // Get the script part

		Pattern p = Pattern.compile("(?is)" + value + "=\"(.+?)\""); // Regex for the value of the key
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html()); // you have to use html here and NOT text! Text will drop the 'key' part

			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + "]";
	}

	/.*
	sessionID:d046430d1d2ac4bce94745b0
	type:profileSave
	weblink_1_title:
	weblink_1_url:
	weblink_2_title:
	weblink_2_url:
	weblink_3_title:
	weblink_3_url:
	personaName:WolverinDEVs
	real_name:WolverinGER
	country:
	state:
	city:
	customURL:WolverinDEV
	summary:Keine Informationen angegeben.
	favorite_badge_badgeid:2
	favorite_badge_communityitemid:
	primary_group_steamid:10434049
	 *./
}
                                                                                                                                                                                                                                                                                                                               
*//*
-----------------------------------------------
package dev.wolveringer.steam;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import dev.wolveringer.GetSteamProfileData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AccountData {
	private final SteamAccount handle;

	private String nickname;
	private String realname;
	private String urlName;
	private String country;
	private String summary;
	private String primaryGroupId;
	private String visiableBadgeId;
	private String privateSteamId;

	public void request() throws Exception {
		GetSteamProfileData data = new GetSteamProfileData(handle.getUsername(), handle.getSessionId(), handle.getLoginToken(), handle.getLoginSecure(), handle.getMachineAuth(), handle.getRemomberToken(), handle.getClient(), handle.getHttpConfig());
		data.get();
		Document doc = Jsoup.parse(data.getResponse());
		nickname = doc.getElementById("personaName").val(); //real_name
		realname = doc.getElementById("real_name").val(); //customURL
		urlName = doc.getElementById("customURL").val(); //summary
		summary = doc.getElementById("summary").val(); //summary
		visiableBadgeId = doc.getElementById("favorite_badge_badgeid").val(); //summary primary_group_steamid
		primaryGroupId = doc.getElementById("primary_group_steamid").val(); //summary	
		primaryGroupId = doc.getElementById("primary_group_steamid").val();

		privateSteamId = getVariable(doc, "g_steamID");
		//g_steamID = "76561198309269484";
	}

	private String getVariable(Document doc, String value) {
		Iterator<Element> script = doc.select("script").listIterator(); // Get the script part

		Pattern p = Pattern.compile("(?is)" + value + "=\"(.+?)\""); // Regex for the value of the key
		while (script.hasNext()) {
			Element element = script.next();
			Matcher m = p.matcher(element.html()); // you have to use html here and NOT text! Text will drop the 'key' part
			System.out.println(element.html());
			while (m.find()) {
				return m.group(1);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "AccountData [handle=" + handle + ", nickname=" + nickname + ", realname=" + realname + ", urlName=" + urlName + ", country=" + country + ", summary=" + summary + ", primaryGroupId=" + primaryGroupId + ", visiableBadgeId=" + visiableBadgeId + ", privateSteamId=" + privateSteamId + "]";
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
*/