package dev.wolveringer;

public enum LoginState {
	SUCCESS,
	WRONG_PASSWORD,
	TWO_FACTOR_AUTH,
	WRONG_CAPTCHA,
	LOGIN_BLOCKED,
	EMAIL_AUTH,
	UNKNOWN;
}
